# OpenML dataset: Chennai-Zomato-Restaurants-Data

https://www.openml.org/d/43536

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a tribute to my favourite city's rich food scene
Content
Entirely derived from web-scraping Zomato Chennai page. The data has important columns like the price for 2, top-selling dishes and cuisines the restaurant serve. It also has Zomato given ratings and how many votes were used to come up with that rating. Any field which I was unable to scrape is given as 'invalid'
Acknowledgements
All of the data belongs to Zomato and Zomato only. I thank Zomato for their ever helpful website. This data should be used only for academic purposes
Inspiration
I'm looking at how prices vary across Chennai's areas and which cuisine is famous in which areas and do they verify the gentrification of certain areas in Chennai. More interested in these sorts of things.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43536) of an [OpenML dataset](https://www.openml.org/d/43536). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43536/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43536/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43536/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

